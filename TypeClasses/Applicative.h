#define ApplicativeInstance(T, A, B, R, P, TP, V1, PR, V2, F, AP)     \
TypeClass(Applicative(T, B), T A)                                     \
IsA(T B,  Functor(T, A))                                              \
T A Pure(T, A, B)(TP V1) {                                            \
    PR                                                                \
}                                                                     \
T B Apply(T, A, B)(T A V2, T(ApplicativeFunction(T, A, B, R, P)) F) { \
    AP                                                                \
}
#define Applicative(T, A)                      Name(T A, Applicative)
#define ApplicativeFunction(T, A, B, R, P)     Name(Name(Name(T A,  \
                                                              T B), \
                                                         Name(R,    \
                                                              P)),  \
                                                    ApplicativeF)
#define ApplicativeFunctionType(T,                                         \
                                A,                                         \
                                B,                                         \
                                R,                                         \
                                P) FunctionType(R, ApplicativeFunction(T,  \
                                                                       A,  \
                                                                       B,  \
                                                                       R,  \
                                                                       P), \
                                                (P))
#define Pure(T, A, B)                    Name(Name(T A, T B), Pure)
#define Apply(T, A, B)                   Name(Name(T A, T B), Apply)
