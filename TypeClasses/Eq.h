#define EqInstance(T, A, B, EQ) \
TypeClass(Eq, T)                \
bool Equals(T)(T A, T B) {      \
   EQ                           \
}
#define Equals(T) Name(T, Equals)
