#include <stdio.h>
#include <stdlib.h>
#include "TypeClasses/Base.h"
#include "Types/List.h"

ListType(char, 0)

int main(void) {
    List(char) Foo = Cons(char)(10, Nil(char));
    List(char) Bar = Copy(char)(Foo, Nil(char));
    printf("%d\n", Head(char)(Foo));
    printf("%d\n", Head(char)(Bar));
    Free(char)(Foo);
    Free(char)(Bar);
    return 0;
}
