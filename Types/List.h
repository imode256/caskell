#define ListType(T, D)                         \
typedef struct List(T) List(T);                \
struct List(T) {                               \
    T        Head;                             \
    List(T)* Tail;                             \
    List(T)* Self;                             \
};                                             \
List(T) Nil(T) {                               \
    List(T) result;                            \
    result.Tail = ((void*)0);                  \
    result.Self = ((void*)0);                  \
    return result;                             \
}                                              \
List(T) Cons(T)(T       head,                  \
                List(T) tail) {                \
    List(T)* result;                           \
    result = Allocate(sizeof(List(T)));        \
    if(result == ((void*)0)) {                 \
        return Nil(T);                         \
    }                                          \
    result->Head = head;                       \
    result->Tail = tail.Self;                  \
    result->Self = result;                     \
    return *result;                            \
}                                              \
bool IsNil(T)(List(T) list) {                  \
    return (                                   \
               list.Self == ((void*)0)         \
           );                                  \
}                                              \
bool IsEnd(T)(List(T) list) {                  \
    return (                                   \
               list.Tail == ((void*)0)         \
           );                                  \
}                                              \
T Head(T)(List(T) list) {                      \
    return (IsNil(T)(list)) ?                  \
           (D) :                               \
           (list.Head);                        \
}                                              \
List(T) Tail(T)(List(T) list) {                \
    return (IsEnd(T)(list)) ?                  \
           (Nil(T)) :                          \
           (*list.Tail);                       \
}                                              \
List(T) End(T)(List(T) list) {                 \
    if(IsEnd(T)(list)) {                       \
        return list;                           \
    }                                          \
    else {                                     \
        return End(T)(*list.Tail);             \
    }                                          \
}                                              \
List(T) Concatenate(T)(List(T) list1,          \
                       List(T) list2) {        \
    if(IsNil(T)(list1)) {                      \
        return list2;                          \
    }                                          \
    else {                                     \
        End(T)(list1).Self->Tail = list2.Self; \
        list1.Tail = list2.Self;               \
        return list1;                          \
    }                                          \
}                                              \
List(T) Copy(T)(List(T) list,                  \
                List(T) accumulator) {         \
    if(IsNil(T)(list)) {                       \
        return Nil(T);                         \
    }                                          \
    else if(IsEnd(T)(list)) {                  \
        return Cons(T)(Head(T)(list),          \
                         accumulator);         \
    }                                          \
    else {                                     \
        return Copy(T)(Tail(T)(list),          \
                       Cons(T)(Head(T)(list),  \
                               accumulator));  \
    }                                          \
}                                              \
List(T) Erase(T)(List(T) list) {               \
    if(IsNil(T)(list)) {                       \
        return Nil(T);                         \
    }                                          \
    else if(IsEnd(T)(list)){                   \
        Release(list.Self);                    \
        return Nil(T);                         \
    }                                          \
    else {                                     \
        Release(list.Self);                    \
        return *list.Tail;                     \
    }                                          \
}                                              \
List(T) Free(T)(List(T) list) {                \
    List(T)* iterator;                         \
    if(IsNil(T)(list)) {                       \
        return Nil(T);                         \
    }                                          \
    else if(IsEnd(T)(list)) {                  \
        Release(list.Self);                    \
        return Nil(T);                         \
    }                                          \
    else {                                     \
        iterator = list.Tail;                  \
        Release(list.Self);                    \
        return Free(T)(*iterator);             \
    }                                          \
}
#define ListFold(T, R)                         \
R Fold(T, R)(List(T) list,                     \
             T       initial,                  \
             R       (*function)(T, T)) {      \
    T        head;                             \
    List(T)* tail;                             \
    if(IsNil(T)(list)) {                       \
        return initial;                        \
    }                                          \
    else if(IsEnd(T)(list)) {                  \
        head = list.Head;                      \
        Release(list.Self);                    \
        return function(head,                  \
                        Fold(T, R)(Nil(T),     \
                                   initial,    \
                                   function)); \
    }                                          \
    else {                                     \
        head = list.Head;                      \
        tail = list.Tail;                      \
        Release(list.Self);                    \
        return function(head,                  \
                        Fold(T, R)(*tail,      \
                                   initial,    \
                                   function)); \
    }                                          \
}
#define Nil(T)         Name(T, Nil)()
#define Cons(T)        Name(T, Cons)
#define IsNil(T)       Name(T, IsNil)
#define IsEnd(T)       Name(T, IsEnd)
#define Head(T)        Name(T, Head)
#define Tail(T)        Name(T, Tail)
#define End(T)         Name(T, End)
#define Concatenate(T) Name(T, Concatenate)
#define Fold(T, R)     Name(T, Name(R, Fold))
#define Copy(T)        Name(T, Copy)
#define Erase(T)       Name(T, Erase)
#define Free(T)        Name(T, Free)
#define List(T)        Name(T, List)
