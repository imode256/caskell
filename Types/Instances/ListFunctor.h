#define ListFunctor(T, R)                               \
FunctorInstance(List, (T), (R), T, R, list, function, { \
    List(R) result;                                     \
    result = (IsEnd(T)(list) == true) ?                 \
             (Cons(R)(function(list.Head), Nil(R))) :   \
             (Cons(R)(function(list.Head),              \
                      Map(List, (T), (R))(*list.Tail,   \
                                          function)));  \
    free(list.Self);                                    \
    return result;                                      \
})
