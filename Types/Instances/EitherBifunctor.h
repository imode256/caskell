#define EitherBifunctor(A, B, C, D)            \
BifunctorInstance(Either,                      \
               (A, B),                         \
               (C, D),                         \
               A,                              \
               C,                              \
               B,                              \
               D,                              \
               either,                         \
               left,                           \
               right, {                        \
    return (either.IsLeft) ?                   \
           (Left(C, D)(left(either.Left))) :   \
           (Right(C, D)(right(either.Right))); \
})
