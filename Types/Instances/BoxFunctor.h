#define BoxFunctor(T, R)                              \
FunctorInstance(Box, (T), (R), T, R, box, function, { \
    return (IsNull(T)(box)) ?                         \
           (Null(R)) :                                \
           (Create(R)(function(box.Value)));          \
})
